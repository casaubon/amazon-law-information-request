# AWS "Law Enforcement Information Request" history

## Goal and objective

Every six months since 2015, Amazon has provided a report recording the _law Enforcement_ requests for data requisitions made to it.

These reports are available at this url: https://www.amazon.com/gp/help/customer/display.html?nodeId=GYSDRGWQ2C2CRYEF

This Git repository and other resources aim to track and monitor this data.  

## How it works?

### Resources
Available resources are:
* This Git repository, containing in particular:
    * All data provided by AWS since 2015, distributed in 4 CSV files
    * A CI/CD pipeline to update BigQuery tables
* [4 BigQuery datasets](https://console.cloud.google.com/bigquery?project=amazonlawinformationrequest&ws=!1m0) containing the same data
* 1 Lookerstudio dashboard (https://lookerstudio.google.com/u/0/reporting/320d398c-9a8c-4de9-80cf-9d390912b033)
* 1 blog post describing Amazon data and reports, available in French on timspirit.fr ([Link](https://timspirit.fr/articles/historique-des-requisitions-judiciaires-de-donnees-chez-amazon-et-aws-law-enforcement-information-requests/)) and in English on Medium ([Link](https://medium.com/timspirit/history-of-law-enforcement-information-requests-at-amazon-and-aws-c0fcb478117b))

### Data
Raw data from Amazon reports are stored in these 4 files in the `rawdata` directory:
* `before2020-Informationrequests-TS.csv`: contains all data in Amazon's format before the 2020 breaking changes.
    * `Date`: this date marks the end of the half-year period in question (e.g. `2019-12-31` refers to the second half of 2019).
    * `Entity` : Entity to which the request has been sent.  
    Possible values: `AWS`, `Amazon`.
    * `Request`: Type of request made to Amazon (for more details, see blog posts).  
  Possible values: `Non-US requests`, `Other court orders`, `Request`, `Search warrants`, `Subpoenas received`.
    * `Response`: Type of response provided by Amazon (for more details, see blog posts).  
    Possible values: `Full response`, `No response`, `Partial response`, `Response`
    * `QTY` : the number of requests received by Amazon corresponding to the dimensions
* `after2020-RequestsHistory-TS.csv`: number of requests per requesting country.
  * `Date`: this date marks the end of the period in question (e.g. `2019-12-31` refers to the second half of 2019).
  * `Entity`: Entity to which the request has been sent.  
  Possible values: `AWS`, `Amazon`.
  * `CountryOrigin`: Country from which the request was made to Amazon.  
  Possible values: [country code ISO 3166](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)
  * `QTY` : the number of requests received by Amazon corresponding to the dimensions
* `after2020-ResponsesType-TS.csv`: type of responses provided by Amazon.
  * `Date` : this date marks the end of the half-year period in question (e.g. `2019-12-31` refers to the second half of 2019).
  * `Entity`: Entity to which the request has been sent.  
  Possible values: `AWS`, `Amazon`.
  * `ResponseType`: The type of response provided (for more details, see blog posts).  
  Possible values: `Content`, `Non-Content`. 
  * `QTY` : the number of requests received by Amazon corresponding to the dimensions.
* `after2020-DisclosureOutsideUS-TS.csv` : 
  * `Date`: this date marks the end of the half-year period in question (e.g. `2019-12-31` refers to the second half of 2019)
  * `Disclosure`: the title of the question as formulated in Amazon's reports.  
  Value: `How many requests resulted in the disclosure to the U.S. government of enterprise content data located outside the United States?`
  * `QTY`: the number of requests received by Amazon corresponding to the dimensions

### Deployment

The pipeline loads data from CSV files into BigQuery tables in 3 steps:  
  1. `validate` : validates the format of CSV files contained in the `rawdata` directory. The only verification today is to check that the number of separators is identical on all lines.
  1. `listfiles` : not all files are loaded by default. This job detects files that have been modified in the current commit and saves them in the `updatedrawdata.txt` file, which is then saved as _artifact_ GitLab for the next job.
      * If the `FORCE_RELOAD` variable is passed to the job with the value `true`, then all CSV files will be loaded into BigQuery tables.
  1. `deploy` : executes a _bq load_ of each CSV file listed in the `updatedrawdata.txt` artifact in its corresponding BigQuery table (if `FORCE_RELOAD=true`, all tables are loaded).
  The mapping between files and tables is as follows:
      * `rawdata/after2020-DisclosureOutsideUS-TS.csv` is loaded into `2-LawEnforcementDisclosureOutsideTheUS-TS`
      * `rawdata/after2020-RequestsHistory-TS.csv` is loaded into `2-LawEnforcementRequest-TS`
      * `rawdata/after2020-ResponsesType-TS.csv` is loaded into `2-LawEnforcementResponseType-TS`
      * `rawdata/before2020-Informationrequests-TS.csv` is loaded into `1-LawEnforcementRequest-TS`

#### Google Cloud Environment
BigQuery tables are in the GCP project [`amazonlawinformationrequest`](https://console.cloud.google.com/bigquery?project=amazonlawinformationrequest&ws=!1m0) :
  * dataset `FullLawInformationRequest` for production (i.e branch is `main`)
  * dataset `FullLawInformationRequestDev` for dev and test (i.e branch is not `main`)

## Updates
The data will be updated with new Amazon publications every six months.

## Author
This repo is maintained by Antoine Lagier ([LinkedIn profile](https://www.linkedin.com/in/alagier/)).