#!/bin/bash

# check artifact
echo "check artifact : ls -l updatedrawdata.txt"
ls -l updatedrawdata.txt

# auth with SA
echo SAGCPMAIL=${SAGCPMAIL}
echo SAGCP=${SAGCP}
echo GCPPROJECT=${GCPPROJECT}
echo "--- auth glcoud with SA..."
gcloud auth activate-service-account ${SAGCPMAIL} --key-file=${SAGCP} --project=${GCPPROJECT}

# test access
echo "-- bq ls"
bq ls

# parsing all updated rawdata listed in artifact
ERR=0
GLBERR=0
for file in $(cat updatedrawdata.txt | grep rawdata); do
    echo "--- ${file}"

    case $file in 
    "rawdata/after2020-DisclosureOutsideUS-TS.csv") 
        # file for "Law Enforcement Information Requests" reports after 2020 (disclosure outside US)
        # to be loaded into "2-LawEnforcementDisclosureOutsideTheUS-TS" table
        TARGETTABLE="2-LawEnforcementDisclosureOutsideTheUS-TS"

        # Load
        echo "- bq loading ${file} into \"${TARGETTABLE}\" table..."
        bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=";" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
        ERR=$?
        echo "- bq loaded with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))

        # Description
        echo "- bq updating description of \"${TARGETTABLE}\" table..."
        bq update --description "        
Loaded on $(date) from gitlab.
Command : bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=\";\" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
CI_JOB_URL=${CI_JOB_URL:-"not defined"}
CI_PROJECT_URL=${CI_PROJECT_URL:-"not defined"}
" ${DATASET}.${TARGETTABLE}
        ERR=$?
        echo "- bq updated description with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))
        ;;

    "rawdata/after2020-RequestsHistory-TS.csv") 
        # file for "Law Enforcement Information Requests" reports after 2020 (request per country)
        # to be loaded into "2-LawEnforcementRequest-TS" table
        TARGETTABLE="2-LawEnforcementRequest-TS"
        
        # Load
        echo "- bq loading ${file} into \"${TARGETTABLE}\" table..."
        bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=";" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
        ERR=$?
        echo "- bq loaded with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))

        # Description
        echo "- bq updating description of \"${TARGETTABLE}\" table..."
        bq update --description "        
Loaded on $(date) from gitlab.
Command : bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=\";\" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
CI_JOB_URL=${CI_JOB_URL:-"not defined"}
CI_PROJECT_URL=${CI_PROJECT_URL:-"not defined"}
" ${DATASET}.${TARGETTABLE}
        ERR=$?
        echo "- bq updated description with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))
        ;;

    "rawdata/after2020-ResponsesType-TS.csv")
        # file for "Law Enforcement Information Requests" reports after 2020 (response Type : Content or Non-content)
        # to be loaded into "2-LawEnforcementResponseType-TS" table
        TARGETTABLE="2-LawEnforcementResponseType-TS"

        # Load
        echo "- bq loading ${file} into \"${TARGETTABLE}\" table..."
        bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=";" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
        ERR=$?
        echo "- bq loaded with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))

        # Description
        echo "- bq updating description of \"${TARGETTABLE}\" table..."
        bq update --description "        
Loaded on $(date) from gitlab.
Command : bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=\";\" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
CI_JOB_URL=${CI_JOB_URL:-"not defined"}
CI_PROJECT_URL=${CI_PROJECT_URL:-"not defined"}
" ${DATASET}.${TARGETTABLE}
        ERR=$?
        echo "- bq updated description with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))
        ;;

    "rawdata/before2020-Informationrequests-TS.csv")
        # file for "Law Enforcement Information Requests" reports from 2015 to 2020
        # to be loaded into "1-LawEnforcementRequest-TS" table
        TARGETTABLE="1-LawEnforcementRequest-TS"

        # Load
        echo "- bq loading ${file} into \"${TARGETTABLE}\" table..."
        bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=";" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
        ERR=$?
        echo "- bq loaded with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))

        # Description
        echo "- bq updating description of \"${TARGETTABLE}\" table..."
        bq update --description "        
Loaded on $(date) from gitlab.
Command : bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=\";\" --source_format=CSV ${DATASET}.${TARGETTABLE} $file
CI_JOB_URL=${CI_JOB_URL:-"not defined"}
CI_PROJECT_URL=${CI_PROJECT_URL:-"not defined"}
" ${DATASET}.${TARGETTABLE}
        ERR=$?
        echo "- bq updated description with exitcode=${ERR}"
        GLBERR=$(($GLBERR+$ERR))
        ;;

    "rawdata/tmp.csv")
        # table test pour CI
        if [[ $ENV = "prod" ]]; then
            echo "- pas de test avec tmp.csv en prod"
        else
            echo "- bq loading ${file} into \"3-tmp\" table (env=${ENV})..."
            bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=";" --source_format=CSV ${DATASET}.3-tmp $file
            ERR=$?
            echo "- bq loaded with exitcode=${ERR}"
            GLBERR=$(($GLBERR+$ERR))

            echo "- bq updating description of \"3-tmp\" table..."
            bq update --description "Loaded on $(date) from gitlab.
Command : bq load --replace=true -skip_leading_rows=1 --autodetect=false --field_delimiter=\";\" --source_format=CSV ${DATASET}.3-tmp $file
CI_JOB_URL=${CI_JOB_URL:-"not defined"}
CI_PROJECT_URL=${CI_PROJECT_URL:-"not defined"}
" ${DATASET}.3-tmp
            ERR=$?
            echo "- bq updated description with exitcode=${ERR}"
            GLBERR=$(($GLBERR+$ERR))
        fi
        ;;

    *)
        # pas de correspondance
        echo "pas de correspondance de fichier avec une table BQ"
        GLBERR=$(($GLBERR+1))
        ;;

    esac

done

exit $GLBERR
