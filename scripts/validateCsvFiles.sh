#!/bin/bash

RAWDATAFOLDER="./rawdata/"
echo "Raw data folder : $RAWDATAFOLDER"

ERR=0
GLBERR=0

for file in $(ls -1 $RAWDATAFOLDER/*); do
    [ $(awk -F\; "{ print NF }" $file | uniq | wc -l) -ne 1 ] \
        && ERR=1  

    echo -n "$file"
    if [[ $ERR -eq 0 ]]; then
        echo " (no error) : $(awk -F\; "{ if(NR==1) print NF }" $file) separators"
    else
        echo " (error) : bad formatted CSV"
        GLBERR=$(($GLBERR+$ERR))
    fi

    ERR=0
done

exit $GLBERR

