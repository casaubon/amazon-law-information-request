
clean:
	@date
	@echo "Clean"

validate:
	@date
	@echo "Validate data files"
	./scripts/validateCsvFiles.sh

listfiles:
	@date
	@echo "List updated rawdata files"
	echo CI_COMMIT_BEFORE_SHA=${CI_COMMIT_BEFORE_SHA}
	echo CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}
	echo CI_COMMIT_SHA=${CI_COMMIT_SHA}
	echo FORCE_RELOAD=${FORCE_RELOAD}
	git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHORT_SHA} > updatedrawdata.txt 
	# si on force la variable dans le pipeline, on recharge tout
	if [ "${FORCE_RELOAD}" = "true" ]; then \
		ls -1 rawdata/* > updatedrawdata.txt; \
	fi
	cat updatedrawdata.txt

load:
	@date
	@echo "Load tables"
	./scripts/load.sh






